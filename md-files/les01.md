[Readme](/Readme.md) - Connection

[Les 01](/md-files/les01.md/les01.md) - SELECT

[Les 02](/md-files/les02.md) - SELECT / WHERE

[Bijlage 1](/md-files/Bijlage-1.md) - MySQL String Functions

# Les 01 - SELECT

### Jetbrains Datagrip, phpStorm of een andere IDE voor databases gaan we vanaf hier gebruiken

# SELECT * FROM tablename;
Om gegevens uit een database te krijgen die we kunnen gebruiken in een applicatie maken we gebruik van het statement SELECT. Naast gegevens uit een database kunnen we met SELECT ook bijvoorbeeld berekeningen op het scherm laten zien.

```mysql
SELECT 6 * 4; # outputs 24
```

### SELECT field1, field2, field8 FROM tablename; 
Wanneer we een select over een aantal velden van een database doen moeten we de velden scheiden door comma's ','.
```mysql
SELECT 
    contactFirstName,
    contactLastName,
    city
FROM
    customers;
```

### Internal functions MySQL

Naast standaard dingen die gedaan kunnen worden zoals rekenen heeft MySQL nog een aantal internal functies (zie [bijlage 1](/md-files/Bijlage-1.md) dit document)
```mysql
SELECT now();
SELECT CONCAT('Stephan' 'Hoeksema') AS name; # Concatenate two fields
```

## Assignment ##
* Show your name and place of birth in one sting with label 'Profile'
* Show 5 other internal functions



# Order BY
```mysql
SELECT
    contactFirstName,
    contactLastName,
    city
FROM
    customers
ORDER BY
    contactFirstName desc;
```

## Assignment ##
Select all employees, concat first and last name as fullname, email as digital address. Order by jobtitle make sure the President of the company comes first!



