[Readme](/Readme.md)

[Les 01](/md-files/les01.md) - Connection / SELECT

[Les 02](/md-files/les02.md/les02.md) - SELECT / WHERE

[Bijlage 1](/md-files/Bijlage-1.md) - MySQL String Functions

# Les 02 - SELECT / WHERE

Met de WHERE kunnen we een zoekcriteria mee geven bij een query zodat we alleen de gegevens terug krijgen die aan die voorwaarde voldoen. 

```mysql
SELECT
    select_list
FROM
    table_name
WHERE
    search_condition;
```
De zoekcriteria kunnen er ook meer zijn in een Query ze kunnen gecombineerd worden met AND, OR of NOT.

Het SELECT statement geeft de waardes terug die voldoen aan de voorwaarde.


![](/images/where.png)


De volgorde van het uitvoeren van een SELECT statement staat hierboven. Eerst wordt er gekeken naar welke tabel, dan de voorwaarden, dan de query en dan de order by.

Example
```mysql
SELECT 
    lastname, 
    firstname, 
    jobtitle
FROM
    employees
WHERE
    jobtitle = 'Sales Rep';
```


## Assignment
* Maak een query op producten waar je product naam laat zien,
* de prijs per stuk en waar de de scale 1:18 is.

## Assignment
* Maak een query op producten waar je product naam laat zien,
* de prijs per stuk en waar de de scale 1:24 en Gearbox Collectibles
* de producent is##

## Assignment ##
* Maak een query op producten waar je product naam laat zien, de prijs per stuk en waar de de scale 1:24 en Gearbox Collectibles of Autoart Studio Design de producent is


## Assignment ##
* Maak een query op producten waar je product naam laat zien, de prijs per stuk en waar de de scale 1:24 is en de price tussen de 20 en de 30 ligt (BETWEEN) Chev 

## Assignment ##
* Maak een query op producten waar je product naam laat zien, de prijs per stuk en waar de productname begint met `Chev`


De tabel hieronder laat ook de vergeleijding die je kan toepassen op je query's met je where clause

| Operator | Description |
|:---|---|
| =	| Equal to. You can use it with almost any data type. |
| <> or !=	| Not equal to |
| <	| Less than.  You typically use it with numeric and date/time data types.|
| >	| Greater than. |
| <= | Less than or equal to |
| >= | Greater than or equal to |

