[Readme](/Readme.md) - Connection

[Les 01](/md-files/les01.md) - SELECT

[Les 02](/md-files/les02.md) - SELECT / WHERE

[Bijlage 1](/md-files/Bijlage-1.md) - MySQL String Functions

# Terminal
Op de command-line (Terminal voor Mac, Powershell of Promt voor windows) doen we de volgende commando's.

## Assignment ##
### Open de mysql prompt

* ```mysql -u root -p <enter>``` 
* ```password: <wachtwoord voor root>```

### source database
* Download zip
* Unpack zip and login to mysql
* On the command-line:

```mysql 
source C:\waarjedezipfile\uitgepakthebtstaan\classicmodels.sql 
```

# SHOW DATABASES
```mysql 
SHOW DATABASES; 
```

Show dataabases on your server on this moment.

# use database
```mysql 
USE classicmodels; 
```

Use the database for the queries.

# show tables
```mysql 
SHOW TABLES; 
```

Laat de tabellen zien in je database die je op dit momentgebruikt
